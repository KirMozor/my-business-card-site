﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApplication1.Pages;

public class AboutSiteModel : PageModel
{
    private readonly ILogger<AboutSiteModel> _logger;

    public AboutSiteModel(ILogger<AboutSiteModel> logger)
    {
        _logger = logger;
    }

    public void OnGet()
    {
    }
}